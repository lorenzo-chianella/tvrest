package it.pegaso.tvrest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.tvrest.exceptions.AbbonamentoNotFoundException;
import it.pegaso.tvrest.exceptions.CanaleNotFoundException;
import it.pegaso.tvrest.models.Canale;
import it.pegaso.tvrest.repositories.CanaleRepository;

@Service
public class CanaleService {

	@Autowired
	private CanaleRepository canaleRepository;
	
	public List<Canale> getAll(){
		return canaleRepository.findAll();
	}
	
	public Canale getById(Long id) {
		return canaleRepository.findById(id)
				.orElseThrow(() -> new CanaleNotFoundException(HttpStatus.NOT_FOUND, "Canale non trovato"));
	}
	
	public Canale insert(Canale c) {
		return canaleRepository.save(c);
	}
	
	public Canale update(Canale c) {
		if (canaleRepository.findById(c.getId()).isPresent()) {
			return canaleRepository.save(c);
		} else {
			throw new CanaleNotFoundException(HttpStatus.NOT_FOUND, "Canale non trovato, impossibile aggiornare");
		}
	}
	
	public String delete(Long id) {
		if (canaleRepository.findById(id).isPresent()) {
			canaleRepository.deleteById(id);
		} else {
			throw new CanaleNotFoundException(HttpStatus.NOT_FOUND, "Canale non trovato, impossibile eliminare");
		}
		return "Deleted";
	}
}
