package it.pegaso.tvrest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.tvrest.exceptions.ClienteNotFoundException;
import it.pegaso.tvrest.models.Cliente;
import it.pegaso.tvrest.repositories.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;
	
	public List<Cliente> getAll(){
		return clienteRepository.findAll();
	}
	
	public Cliente getById(Long id) {
		return clienteRepository.findById(id)
				.orElseThrow(() -> new ClienteNotFoundException(HttpStatus.NOT_FOUND, "Cliente non trovato"));
	}
	
	public Cliente insert(Cliente c) {
		return clienteRepository.save(c);
	}
	
	public Cliente update(Cliente c) {
		if (clienteRepository.findById(c.getId()).isPresent()) {
			return clienteRepository.save(c);
		} else {
			throw new ClienteNotFoundException(HttpStatus.NOT_FOUND, "Cliente non trovato, impossibile aggiornare");
		}
	}
	
	public String delete(Long id) {
		if (clienteRepository.findById(id).isPresent()) {
			clienteRepository.deleteById(id);
		} else {
			throw new ClienteNotFoundException(HttpStatus.NOT_FOUND, "Cliente non trovato, impossibile eliminare");
		}
		return "Deleted";
	}
}
