package it.pegaso.tvrest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.tvrest.exceptions.AbbonamentoNotFoundException;
import it.pegaso.tvrest.models.Abbonamento;
import it.pegaso.tvrest.repositories.AbbonamentoRepository;

@Service
public class AbbonamentoService {

	@Autowired
	private AbbonamentoRepository abbonamentoRepository;

	public List<Abbonamento> getAll() {
		return abbonamentoRepository.findAll();
	}

	public Abbonamento getById(Long id) {
		return abbonamentoRepository.findById(id)
				.orElseThrow(() -> new AbbonamentoNotFoundException(HttpStatus.NOT_FOUND, "Abbonamento non trovato"));
	}

	public Abbonamento insert(Abbonamento a) {
		return abbonamentoRepository.save(a);
	}

	public Abbonamento update(Abbonamento a) {
		if (abbonamentoRepository.findById(a.getId()).isPresent()) {
			return abbonamentoRepository.save(a);
		} else {
			throw new AbbonamentoNotFoundException(HttpStatus.NOT_FOUND, "Abbonamento non trovato, impossibile aggiornare");
		}
	}

	public String delete(Long id) {
		if (abbonamentoRepository.findById(id).isPresent()) {
			abbonamentoRepository.deleteById(id);
		} else {
			throw new AbbonamentoNotFoundException(HttpStatus.NOT_FOUND, "Abbonamento non trovato");
		}
		return "Deleted";
	}
}
