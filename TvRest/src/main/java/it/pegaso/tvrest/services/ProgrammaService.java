package it.pegaso.tvrest.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import it.pegaso.tvrest.exceptions.ClienteNotFoundException;
import it.pegaso.tvrest.exceptions.ProgrammaNotFoundException;
import it.pegaso.tvrest.models.Programma;
import it.pegaso.tvrest.repositories.ProgrammaRepository;

@Service
public class ProgrammaService {

	@Autowired
	private ProgrammaRepository programmaRepository;
	
	public List<Programma> getAll(){
		return programmaRepository.findAll();
	}
	
	public Programma getById(Long id) {
		return programmaRepository.findById(id)
				.orElseThrow(() -> new ProgrammaNotFoundException(HttpStatus.NOT_FOUND, "Programma non trovato"));
	}
	
	public Programma insert(Programma p) {
		return programmaRepository.save(p);
	}
	
	public Programma update(Programma p) {
		if (programmaRepository.findById(p.getId()).isPresent()) {
			return programmaRepository.save(p);
		} else {
			throw new ProgrammaNotFoundException(HttpStatus.NOT_FOUND, "Programma non trovato, impossibile aggiornare");
		}
	}
	
	public String delete(Long id) {
		if (programmaRepository.findById(id).isPresent()) {
			programmaRepository.deleteById(id);
		} else {
			throw new ProgrammaNotFoundException(HttpStatus.NOT_FOUND, "Programma non trovato, impossibile eliminare");
		}
		return "Deleted";
	}
}
