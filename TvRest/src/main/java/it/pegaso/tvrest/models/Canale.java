package it.pegaso.tvrest.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Canale implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@Column(nullable=false)
	private String nome;
	
	@ManyToOne
	@JsonBackReference
	private Abbonamento abbonamento;
	
	@OneToMany(mappedBy = "canale", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Programma> programmi;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Abbonamento getAbbonamento() {
		return abbonamento;
	}

	public void setAbbonamento(Abbonamento abbonamento) {
		this.abbonamento = abbonamento;
	}

	public List<Programma> getProgrammi() {
		return programmi;
	}

	public void setProgrammi(List<Programma> programmi) {
		this.programmi = programmi;
	}

	@Override
	public int hashCode() {
		return Objects.hash(abbonamento, id, nome, programmi);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Canale other = (Canale) obj;
		return Objects.equals(abbonamento, other.abbonamento) && Objects.equals(id, other.id)
				&& Objects.equals(nome, other.nome) && Objects.equals(programmi, other.programmi);
	}
	
	
}
