package it.pegaso.tvrest.models;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
public class Programma implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String titolo;
	
	@ManyToOne
	@JsonBackReference
	private Canale canale;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTitolo() {
		return titolo;
	}
	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}
	public Canale getCanale() {
		return canale;
	}
	public void setCanale(Canale canale) {
		this.canale = canale;
	}
	@Override
	public int hashCode() {
		return Objects.hash(canale, id, titolo);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Programma other = (Programma) obj;
		return Objects.equals(canale, other.canale) && Objects.equals(id, other.id)
				&& Objects.equals(titolo, other.titolo);
	}
	
	
}
