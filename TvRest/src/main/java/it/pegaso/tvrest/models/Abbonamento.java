package it.pegaso.tvrest.models;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Abbonamento implements Serializable{

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(nullable=false)
	private String descrizione;
	
	@OneToMany(mappedBy = "abbonamento", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Canale> canali;
	
	@OneToMany(mappedBy = "abbonamento", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Cliente> clienti;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescrizione() {
		return descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public List<Canale> getCanali() {
		return canali;
	}

	public void setCanali(List<Canale> canali) {
		this.canali = canali;
	}

	public List<Cliente> getClienti() {
		return clienti;
	}

	public void setClienti(List<Cliente> clienti) {
		this.clienti = clienti;
	}

	@Override
	public int hashCode() {
		return Objects.hash(canali, clienti, descrizione, id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Abbonamento other = (Abbonamento) obj;
		return Objects.equals(canali, other.canali) && Objects.equals(clienti, other.clienti)
				&& Objects.equals(descrizione, other.descrizione) && Objects.equals(id, other.id);
	}
	
	
	
}
