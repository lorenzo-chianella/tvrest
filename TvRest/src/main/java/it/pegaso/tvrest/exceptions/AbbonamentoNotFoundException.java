package it.pegaso.tvrest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class AbbonamentoNotFoundException extends ResponseStatusException{

	public AbbonamentoNotFoundException(HttpStatus status, String reason) {
		super(status, reason);
	}

}
