package it.pegaso.tvrest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class CanaleNotFoundException extends ResponseStatusException{

	public CanaleNotFoundException(HttpStatus status, String reason) {
		super(status, reason);
	}
}
