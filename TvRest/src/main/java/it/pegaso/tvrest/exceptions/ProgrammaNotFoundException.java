package it.pegaso.tvrest.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class ProgrammaNotFoundException extends ResponseStatusException{

	public ProgrammaNotFoundException(HttpStatus status, String reason) {
		super(status, reason);
		// TODO Auto-generated constructor stub
	}

}
