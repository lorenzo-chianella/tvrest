package it.pegaso.tvrest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TvrestApplication {

	public static void main(String[] args) {
		SpringApplication.run(TvrestApplication.class, args);
	}

}
