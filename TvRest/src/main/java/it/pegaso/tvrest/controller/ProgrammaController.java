package it.pegaso.tvrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.tvrest.models.Canale;
import it.pegaso.tvrest.models.Programma;
import it.pegaso.tvrest.services.ProgrammaService;

@RestController
@RequestMapping("/api/v1/programmi")
public class ProgrammaController {

	@Autowired
	private ProgrammaService programmaService;
	
	@GetMapping("/all")
	public ResponseEntity<List<Programma>> getAll(){
		List<Programma> result = programmaService.getAll();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Programma> getById(@PathVariable("id") Long id){
		Programma prog = programmaService.getById(id);
		return new ResponseEntity<>(prog, HttpStatus.OK);
	}
	
	@PostMapping(value="/save", consumes={"application/json"})
	public ResponseEntity<Programma> insert(@RequestBody Programma p){
		Programma insert = programmaService.insert(p);
		return new ResponseEntity<>(insert, HttpStatus.CREATED);
	}
	
	@PutMapping(value="/update", consumes={"application/json"})
	public ResponseEntity<Programma> update(@RequestBody Programma p){
		Programma update = programmaService.update(p);
		return new ResponseEntity<>(update,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Long id){
		String delete = programmaService.delete(id);
		return new ResponseEntity<String>(delete, HttpStatus.OK);
	}
}
