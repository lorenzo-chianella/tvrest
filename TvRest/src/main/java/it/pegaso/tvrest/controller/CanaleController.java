package it.pegaso.tvrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.tvrest.models.Canale;
import it.pegaso.tvrest.services.CanaleService;

@RestController
@RequestMapping("/api/v1/canali")
public class CanaleController {

	@Autowired
	private CanaleService canaleService;
	
	@GetMapping("/all")
	public ResponseEntity<List<Canale>> getAll(){
		List<Canale> result = canaleService.getAll();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Canale> getById(@PathVariable("id") Long id){
		Canale can = canaleService.getById(id);
		return new ResponseEntity<>(can, HttpStatus.OK);
	}
	
	@PostMapping(value="/save", consumes={"application/json"})
	public ResponseEntity<Canale> insert(@RequestBody Canale c){
		Canale insert = canaleService.insert(c);
		return new ResponseEntity<>(insert, HttpStatus.CREATED);
	}
	
	@PutMapping(value="/update", consumes={"application/json"})
	public ResponseEntity<Canale> update(@RequestBody Canale c){
		Canale update = canaleService.update(c);
		return new ResponseEntity<>(update,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Long id){
		String delete = canaleService.delete(id);
		return new ResponseEntity<String>(delete, HttpStatus.OK);
	}
}
