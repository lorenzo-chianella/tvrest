package it.pegaso.tvrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.tvrest.models.Canale;
import it.pegaso.tvrest.models.Cliente;
import it.pegaso.tvrest.services.ClienteService;

@RestController
@RequestMapping("/api/v1/clienti")
public class ClienteController {

	@Autowired
	private ClienteService clienteService;
	
	@GetMapping("/all")
	public ResponseEntity<List<Cliente>> getAll(){
		List<Cliente> result = clienteService.getAll();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Cliente> getById(@PathVariable("id") Long id){
		Cliente cli = clienteService.getById(id);
		return new ResponseEntity<>(cli, HttpStatus.OK);
	}
	
	@PostMapping(value="/save", consumes={"application/json"})
	public ResponseEntity<Cliente> insert(@RequestBody Cliente c){
		Cliente insert = clienteService.insert(c);
		return new ResponseEntity<>(insert, HttpStatus.CREATED);
	}
	
	@PutMapping(value="/update", consumes={"application/json"})
	public ResponseEntity<Cliente> update(@RequestBody Cliente c){
		Cliente update = clienteService.update(c);
		return new ResponseEntity<>(update,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Long id){
		String delete = clienteService.delete(id);
		return new ResponseEntity<String>(delete, HttpStatus.OK);
	}
}
