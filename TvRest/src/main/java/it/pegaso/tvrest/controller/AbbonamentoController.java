package it.pegaso.tvrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.pegaso.tvrest.models.Abbonamento;
import it.pegaso.tvrest.services.AbbonamentoService;

@RestController
@RequestMapping("/api/v1/abbonamenti")
public class AbbonamentoController {

	@Autowired
	private AbbonamentoService abbonamentoService;
	
	@GetMapping("/all")
	public ResponseEntity<List<Abbonamento>> getAll(){
		List<Abbonamento> result = abbonamentoService.getAll();
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Abbonamento> getById(@PathVariable("id") Long id){
		Abbonamento abb = abbonamentoService.getById(id);
		return new ResponseEntity<>(abb, HttpStatus.OK);
	}
	
	@PostMapping(value="/save", consumes={"application/json"})
	public ResponseEntity<Abbonamento> insert(@RequestBody Abbonamento a){
		Abbonamento insert = abbonamentoService.insert(a);
		return new ResponseEntity<>(insert, HttpStatus.CREATED);
	}
	
	@PutMapping(value="/update", consumes={"application/json"})
	public ResponseEntity<Abbonamento> update(@RequestBody Abbonamento a){
		Abbonamento update = abbonamentoService.update(a);
		return new ResponseEntity<>(update,HttpStatus.CREATED);
	}
	
	@DeleteMapping("/delete/{id}")
	public ResponseEntity<String> delete(@PathVariable("id") Long id){
		String delete = abbonamentoService.delete(id);
		return new ResponseEntity<String>(delete, HttpStatus.OK);
	}
}
