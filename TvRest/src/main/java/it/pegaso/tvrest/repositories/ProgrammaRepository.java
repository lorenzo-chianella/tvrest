package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.tvrest.models.Programma;

@Repository
public interface ProgrammaRepository extends JpaRepository<Programma, Long>{}
