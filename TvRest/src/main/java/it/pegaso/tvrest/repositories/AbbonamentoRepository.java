package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.tvrest.models.Abbonamento;

@Repository
public interface AbbonamentoRepository extends JpaRepository<Abbonamento, Long>{}
