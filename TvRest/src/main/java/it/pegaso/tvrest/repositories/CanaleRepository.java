package it.pegaso.tvrest.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.pegaso.tvrest.models.Canale;

@Repository
public interface CanaleRepository extends JpaRepository<Canale, Long>{}
